const express = require('express');
const app = express();
const cors = require('cors');
require('dotenv/config');

// Middlewares
app.use(cors());
app.use(express.json());

// Routes
const postRouter = require('./routes/posts');
app.use('/posts', postRouter);

// Mongoose connect
const mongoose = require('mongoose');
mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true },
    () => console.log("You are connected to MongoDB")
);

// Port listen
app.listen(process.env.PORT);