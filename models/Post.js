const mongoose = require('mongoose');
const dateformat = require('dateformat');

const now = new Date();
const presentDate = dateformat(now, "fullDate");

const PostSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    date: {
        type: String,
        default: presentDate
    },
    description: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Post', PostSchema);