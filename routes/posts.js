const express = require('express');
const router = express.Router();
const Post = require('../models/Post');
const dateformat = require('dateformat');

// Create new post
router.post('/', async (req,res) => {
    const date = dateformat(req.body.date, "fullDate")
    const newPost = new Post({
        title: req.body.title,
        description: req.body.description,
        date: date
    })

    try {
        const savedPost = await newPost.save();
        res.json(savedPost);
    } catch (error) {
        res.json({ message: error });
    }

})

// Get all post
router.get('/', async(req,res) => {
    try {
        const allPosts = await Post.find();
        res.send(allPosts);
    } catch (error) {
        res.send({ message: error });
    }
});

// Get specific post
router.get('/:postId', async(req,res) => {
    try {
        const allPosts = await Post.find({ _id: req.params.postId });
        res.send(allPosts);
    } catch (error) {
        res.send({ message: error });
    }
})

// Update post
router.patch('/:postId', async(req,res) => {
    if(req.body.title && req.body.description) {  // Update title & description
        try{
            const editedPost = await Post.updateOne( {_id: req.params.postId } ,
            {
                $set: {
                    title: req.body.title,
                    description: req.body.description
                }
            });
            res.send(editedPost);
        } catch (error) {
            res.send({ message: error });
        }
    } else if(req.body.title && !req.body.desctiption) { // Update title only
        try{
            const editedPost = await Post.updateOne( {_id: req.params.postId } ,
            {
                $set: { title: req.body.title }
            });
            res.send(editedPost);
        } catch (error) {
            res.send({ message: error });
        }
    } else if(!req.body.title && req.body.description) { // Update description only
        try{
            const editedPost = await Post.updateOne( {_id: req.params.postId } ,
            {
                $set: { description: req.body.description }
            });
            res.send(editedPost);
        } catch (error) {
            res.send({ message: error });
        }
    } else {
        res.send("update failed");
    }
})

// Delete post
router.delete('/:postId', async(req,res) => {
    if(req.params.postId === "all") {
        try {
            const deletedPost = await Post.deleteMany();
            res.send(deletedPost);
        } catch (error) {
            res.send({ message: error });
        }
    } else {
        try {
            const deletedPost = await Post.remove({ _id: req.params.postId });
            res.send(deletedPost);
        } catch (error) {
            res.send({ message: error });
        }
    }
    
})


module.exports = router;